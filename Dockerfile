FROM node:latest

WORKDIR /usr/src/server

ENV MONGO_URL='mongodb+srv://ron:ronnguyen166@cluster0.ajvel.mongodb.net/social-media'
ENV JWT_SECRET='somesuperhardstringtoguess'
ENV PORT=3001


COPY ["package.json","package-lock.json*", "./"]

COPY . .

RUN npm install

EXPOSE 3001

CMD ["npm", "start"]
